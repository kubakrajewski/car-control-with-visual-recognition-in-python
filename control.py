#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  4 16:52:21 2022

@author: kuba

This package contains helper functions for controlling the car
"""

import time
from cars import Camera, Motors, Connection, Direction

# Forces for moving forward and turning
F_FORW = 75
F_TURN = 61

# Do all necessary things to connect with car
def connect():
    connection = Connection()
    cam = Camera(connection=connection)
    motors = Motors(connection=connection, motor2_multiplier=1, verbose=False)
    
    return connection, cam, motors

def camera_wait(cam, seconds):
    t1 = time.time()
    t2 = time.time()
    while t2 - t1 < seconds:
        cam.get_frame()
        t2 = time.time()
        
# Helper functions for moving car by n steps in a given direction
def forward(cam, mot, n_steps):
    for i in range(n_steps):
        mot.command(F_FORW, Direction.FORWARD)
        camera_wait(cam, 0.1)
        
def right(cam, mot, n_steps):
    for i in range(n_steps):
        mot.command(F_TURN, Direction.RIGHT)
        camera_wait(cam, 0.1)
        
def left(cam, mot, n_steps):
    for i in range(n_steps):
        mot.command(F_TURN, Direction.LEFT)
        camera_wait(cam, 0.1)
        
def back(cam, mot, n_steps):
    for i in range(n_steps):
        mot.command(F_FORW, Direction.BACKWARD)
        camera_wait(cam, 0.1)
        