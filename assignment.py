#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  4 17:16:47 2022

@author: kuba

This module contains final solution of the assignment
"""

import numpy as np
import cv2
from control import connect, camera_wait, forward, left, right, back
from calculations import read_id, get_trans_pitch

MARKER_FRONT_THRESH = 0.03 # threshold for x value to set marker front
N_STEPS_BACK = 4
STEPS_ANGLE_90 = 20
RIGHT_ANGLE = 90/STEPS_ANGLE_90
FORWARD_STEP = 57/20  

#------------------------------ MOTION BLUR -----------------------------------
def motion_blur(cam, mot):
    images = []
    cam.flash_on()
    for i in range(10):
        right(cam, mot, 2)
        images.append(cam.get_frame())
        left(cam, mot, 1)
    cam.flash_off()
        
    for i in range(len(images)):
        cv2.imwrite('photos/img' + str(i) + '.png', images[i])
        

#------------------------------ SIMPLE MARKERS --------------------------------
def simple_markers(cam, mot):
    id_read = read_id(cam)
    
    while not id_read:
        id_read = read_id(cam)
    
    if id_read == 2:
        forward(cam, mot, 10)
    elif id_read == 5:
        right(cam, mot, 10)
    elif id_read == 6:
        left(cam, mot, 10)
    elif id_read == 3:
        back(cam, mot, 10)

#------------------------------ MARKERS IN 3D ---------------------------------
# set marker to the front        
def set_marker_front(cam, mot):        
    while True:
        trans_x, _, _, _ = get_trans_pitch(cam)
    
        while not trans_x:
            trans_x, _, _, _ = get_trans_pitch(cam)
    
        if trans_x > MARKER_FRONT_THRESH:
            right(cam, mot, 1)
        elif trans_x < -MARKER_FRONT_THRESH:
            left(cam, mot, 1)
        else:
            break
    
def go_perpendicular(cam, mot):
    trans_x, trans_y, trans_z, _ = get_trans_pitch(cam)
    
    while not trans_z:
        trans_x, trans_y, trans_z, _ = get_trans_pitch(cam)
    
    while trans_z > 0.2:
        # correct direction and move forward
        set_marker_front(cam, mot)
        forward(cam, mot, 1)
        
        # calculate current position
        trans_x, trans_y, trans_z, _ = get_trans_pitch(cam)
        
        while not trans_z:
            trans_x, trans_y, trans_z, _ = get_trans_pitch(cam)

def set_to_axis(cam, mot):
    set_marker_front(cam, mot)
    trans_x, trans_y, trans_z, pitch_y = get_trans_pitch(cam)
    
    # if we are on the axis return
    if np.abs(pitch_y) < 15:
        return
    
    # calculate angle we have to rotate and number of steps
    angle = 90-np.abs(pitch_y)
    turn_steps = int(np.round(angle/RIGHT_ANGLE))
    
    # calculate distance to move (in centimeters) and number of steps
    segment_right = np.cos(np.radians(angle)) * trans_z * 100
    forward_steps = int(np.round(segment_right/FORWARD_STEP))
    
    # Go to the axis
    if pitch_y < 0:
        right(cam, mot, turn_steps)
        forward(cam, mot, forward_steps)
        camera_wait(cam, 1)
        left(cam, mot, STEPS_ANGLE_90)
    else:
        left(cam, mot, turn_steps)
        forward(cam, mot, forward_steps)
        camera_wait(cam, 1)
        right(cam, mot, STEPS_ANGLE_90)

def go_to_id(cam, mot, id_search):
    id_found = read_id(cam)
    
    # look for the correct marker
    while not id_found or id_found != id_search:
        right(cam, mot, 1)
        id_found = read_id(cam)
        
    # go to the axis
    set_to_axis(cam, mot)
    
    # we are already on the marker's axis. Now we only need to approach it
    go_perpendicular(cam, mot)
    back(cam, mot, N_STEPS_BACK)

def visit_markers(cam, mot, marker_ids):
    for marker_id in marker_ids:
        go_to_id(cam, mot, marker_id) 
        right(cam, mot, int(np.round(STEPS_ANGLE_90/2)))
        
def main():
    con, cam, mot = connect()
    # motion_blur(cam, mot)
    simple_markers(cam, mot)
    # visit_markers(cam, mot, [5, 2, 3, 6])
    
if __name__ == '__main__':
    main()
    