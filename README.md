# Car control with visual recognition in Python
The goal of this project was to use position and orientation of the car
to move between markers. Position and orientation are estimated using pictures 
taken by a camera mounted on the car. The detailed description of the 
project can be found here: https://nomagiclab.github.io/robot-control-course/lab11
