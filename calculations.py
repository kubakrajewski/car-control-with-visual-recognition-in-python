#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  4 16:54:47 2022

@author: kuba

This module contains helper functions needed for calculations etc. and not
necessarily for moving car
"""

import cv2
from scipy.spatial.transform import Rotation as R
import math
from control import camera_wait

MARKER_SIDE = 0.042
CALIB_FILENAME = 'calibration_chessboard.yaml'
DICT = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_APRILTAG_16h5)
        
# Read distortion coefficients
def get_dist_coefs():
    cv_file = cv2.FileStorage(CALIB_FILENAME, cv2.FILE_STORAGE_READ) 
    mtx = cv_file.getNode('K').mat()
    dst = cv_file.getNode('D').mat()
    cv_file.release()
    return (mtx, dst)

# Assuming there is only one marker visible, return its id
def read_id(cam):
    dictionary = DICT
    mtx, dst = get_dist_coefs()
    
    # Wait until camera output is stable
    camera_wait(cam, 0.5)
    
    # get a frame and detect markers
    cam.flash_on()
    frame = cam.get_frame() 
    cam.flash_off()
    (corners, ids, rejected) = cv2.aruco.detectMarkers(
      frame, dictionary,
      cameraMatrix=mtx, distCoeff=dst)
    
    res = ids[0] if ids else None
    
    return res

def get_trans_pitch(cam):
    mtx, dst = get_dist_coefs()
    dictionary = DICT
    
    # Wait until camera output is stable
    camera_wait(cam, 0.5)
        
    cam.flash_on()
    frame = cam.get_frame() 
    cam.flash_off()
     
    # Detect ArUco markers in the video frame
    (corners, marker_ids, rejected) = cv2.aruco.detectMarkers(
      frame, dictionary,
      cameraMatrix=mtx, distCoeff=dst)
       
    # Check that at least one ArUco marker was detected
    if marker_ids is None:
        res = (None, None, None, None)
    else:
      # Get the rotation and translation vectors
      rvecs, tvecs, _ = cv2.aruco.estimatePoseSingleMarkers(
        corners,
        MARKER_SIDE,
        mtx,
        dst)
      
      # Get the pitch angle in radians
      rvecs = rvecs.reshape(3)
      euler = R.from_rotvec(rvecs).as_euler('xyz')
      pitch_y = math.degrees(euler[1])
      
      # Get the translation parameters      
      trans_x = tvecs[0][0][0]
      trans_y = tvecs[0][0][1]
      trans_z = tvecs[0][0][2]
      
      res = (trans_x, trans_y, trans_z, pitch_y)        
        
    return res